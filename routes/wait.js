//express is the framework we're going to use to handle requests
const express = require('express');

//Create a new instance of express router
var router = express.Router();

const bodyParser = require("body-parser");
//This allows parsing of the body of POST requests, that are encoded in JSON
router.use(bodyParser.json());

router.get("/", (req, res) => {
    setTimeout(() => {
        res.send({
            message: "Hello, " + req.query['name'] + "!"
        });
    }, 1000)
});

router.post("/", (req, res) => {
    setTimeout(() => {
        res.send({
            //req.query is a reference to arguments in the POST body
            message: "Hello, " + req.body['name'] + "! You sent a POST Request"
        });
    }, 1000)
});

module.exports = router;